inherited FSaisieGlycemie: TFSaisieGlycemie
  Caption = 'Saise Glyc'#233'mie'
  ClientHeight = 164
  ClientWidth = 342
  OnShow = FormShow
  ExplicitWidth = 358
  ExplicitHeight = 203
  PixelsPerInch = 96
  TextHeight = 14
  inherited Panel1: TPanel
    Top = 131
    Width = 342
    ExplicitTop = 131
    ExplicitWidth = 342
    inherited Btn_Exit: TcxButton
      Left = 208
      ExplicitLeft = 208
    end
    inherited Btn_Ajout: TcxButton
      Left = 69
      OnClick = Btn_AjoutClick
      ExplicitLeft = 69
    end
  end
  object cxDBDateEdit1: TcxDBDateEdit
    Left = 96
    Top = 24
    DataBinding.DataField = 'Date'
    DataBinding.DataSource = DataModule1.DS_DIABETE
    TabOrder = 1
    Width = 121
  end
  object Label_Date: TcxLabel
    Left = 32
    Top = 25
    Caption = 'Date'
  end
  object Label_Valeur: TcxLabel
    Left = 32
    Top = 65
    Caption = 'Valeur'
  end
  object DBEditValeur: TcxDBTextEdit
    Left = 96
    Top = 64
    DataBinding.DataField = 'Valeur'
    DataBinding.DataSource = DataModule1.DS_DIABETE
    TabOrder = 4
    Width = 121
  end
end
