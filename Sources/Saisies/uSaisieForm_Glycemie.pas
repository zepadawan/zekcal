unit uSaisieForm_Glycemie;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uSaisieForm_Custom, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxLabel;

type
  TFSaisieGlycemie = class(TFSaisieFormCustom)
    cxDBDateEdit1: TcxDBDateEdit;
    Label_Date: TcxLabel;
    Label_Valeur: TcxLabel;
    DBEditValeur: TcxDBTextEdit;
    procedure FormShow(Sender: TObject);
    procedure Btn_AjoutClick(Sender: TObject);
    procedure Btn_ExitClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FSaisieGlycemie: TFSaisieGlycemie;

implementation

uses
  uDataModule;

{$R *.dfm}

procedure TFSaisieGlycemie.Btn_AjoutClick(Sender: TObject);
begin
  inherited;
  DataModule1.T_DIABETE.Post;
  Close;
end;

procedure TFSaisieGlycemie.Btn_ExitClick(Sender: TObject);
begin
  DataModule1.T_DIABETE.Delete;
  inherited;
end;

procedure TFSaisieGlycemie.FormShow(Sender: TObject);
begin
  inherited;
  DataModule1.T_DIABETE.Append;
  DataModule1.T_DIABETEDate.Value:= Now;
end;

end.
