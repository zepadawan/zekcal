inherited FSaisieFrame_DIABETE: TFSaisieFrame_DIABETE
  Width = 1136
  Height = 747
  ExplicitWidth = 1136
  ExplicitHeight = 747
  inherited PanelTop: TPanel
    Width = 1136
    Caption = 'Grille des prises de tests de la Glyc'#233'mie'
    ExplicitWidth = 1136
  end
  inherited Panel_Client: TPanel
    Width = 1136
    Height = 669
    ExplicitWidth = 1136
    ExplicitHeight = 669
    inherited Panel_Grid: TPanel
      Width = 1134
      Height = 667
      ExplicitWidth = 1134
      ExplicitHeight = 667
      inherited cxGrid: TcxGrid
        Width = 1132
        Height = 631
        ExplicitWidth = 1132
        ExplicitHeight = 631
        inherited cxGridDBTableView: TcxGridDBTableView
          OnCustomDrawCell = cxGridDBTableViewCustomDrawCell
          DataController.DataSource = DataModule1.DS_DIABETE
          OptionsView.ScrollBars = ssVertical
          OptionsView.CellAutoHeight = True
          OptionsView.ColumnAutoWidth = True
          object cxGridDBTableViewID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
          end
          object cxGridDBTableViewDate: TcxGridDBColumn
            DataBinding.FieldName = 'Date'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Alignment.Horz = taCenter
            Properties.DateButtons = [btnClear, btnNow, btnToday]
            Width = 214
          end
          object cxGridDBTableViewValeur: TcxGridDBColumn
            DataBinding.FieldName = 'Valeur'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            Styles.Content = DataModule1.Gras
            Width = 368
          end
          object cxGridDBTableViewApres_Repas: TcxGridDBColumn
            DataBinding.FieldName = 'Apres_Repas'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taCenter
            Properties.DisplayUnchecked = 'True'
            Properties.NullStyle = nssInactive
            Properties.ShowEndEllipsis = True
            Properties.ValueChecked = '1'
            Properties.ValueUnchecked = '0'
            VisibleForEditForm = bFalse
            Width = 211
          end
        end
      end
      inherited cxDBNavigator: TcxDBNavigator
        Top = 635
        Width = 1125
        DataSource = DataModule1.DS_DIABETE
        ExplicitTop = 635
        ExplicitWidth = 1125
      end
    end
  end
  inherited PanelCBottom: TPanel
    Top = 710
    Width = 1136
    ExplicitTop = 710
    ExplicitWidth = 1136
    inherited Btn_Ajout: TcxButton
      OnClick = Btn_AjoutClick
    end
  end
  inherited Printer: TdxComponentPrinter
    PixelsPerInch = 96
    inherited PrinterLink_Grid: TdxGridReportLink
      PrinterPage.CenterOnPageH = True
      PrinterPage.PageFooter.CenterTitle.Strings = (
        '')
      PrinterPage.PageFooter.Font.Style = [fsBold]
      PrinterPage.PageHeader.CenterTitle.Strings = (
        'Historique des prises Clyc'#233'miques')
      PrinterPage.PageHeader.Font.Color = clBlue
      PrinterPage.PageHeader.Font.Height = -19
      PrinterPage.PageHeader.Font.Style = [fsBold]
      ReportDocument.CreationDate = 44803.343419155090000000
      ReportTitle.Color = clBlue
      ReportTitle.Font.Color = clBlue
      ReportTitle.Font.Height = -29
      OptionsSize.AutoWidth = True
      PixelsPerInch = 96
      BuiltInReportLink = True
    end
  end
  inherited PrinterStyleManager: TdxPrintStyleManager
    PixelsPerInch = 96
    inherited PrinterStyleManagerStyle1: TdxPSPrintStyle
      BuiltInStyle = True
    end
  end
end
