unit uSaisieForm_Custom;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls;

type
  TFSaisieFormCustom = class(TForm)
    Panel1: TPanel;
    Btn_Exit: TcxButton;
    Btn_Ajout: TcxButton;
    procedure Btn_ExitClick(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  FSaisieFormCustom: TFSaisieFormCustom;

implementation

uses
  uDataModule;

{$R *.dfm}

procedure TFSaisieFormCustom.Btn_ExitClick(Sender: TObject);
begin
  Close;
end;

end.
