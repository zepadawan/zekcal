object FSaisieFormCustom: TFSaisieFormCustom
  Left = 0
  Top = 0
  Caption = 'FSaisieFormCustom'
  ClientHeight = 282
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 14
  object Panel1: TPanel
    Left = 0
    Top = 249
    Width = 468
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    ExplicitTop = 216
    ExplicitWidth = 516
    DesignSize = (
      468
      33)
    object Btn_Exit: TcxButton
      Left = 334
      Top = 2
      Width = 133
      Height = 30
      Anchors = [akTop, akRight, akBottom]
      Caption = 'Quitter'
      OptionsImage.ImageIndex = 0
      OptionsImage.Images = DataModule1.ImageList_16
      TabOrder = 0
      OnClick = Btn_ExitClick
    end
    object Btn_Ajout: TcxButton
      Left = 195
      Top = 3
      Width = 133
      Height = 29
      Anchors = [akTop, akRight, akBottom]
      Caption = 'Ajouter'
      OptionsImage.ImageIndex = 1
      OptionsImage.Images = DataModule1.ImageList_16
      TabOrder = 1
    end
  end
end
