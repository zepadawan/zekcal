-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: jfp_db
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_poids`
--

DROP TABLE IF EXISTS `t_poids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_poids` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `Poids` float(9,3) DEFAULT NULL,
  `IMC_ID` int DEFAULT NULL,
  `IMC_Calc` float(9,3) DEFAULT NULL,
  `Ecart_Poids` float(9,3) DEFAULT NULL,
  `Ecart_Cumul` float(9,3) DEFAULT NULL,
  `IMG_Graisse` float(9,3) DEFAULT NULL,
  `IMG_Hydrat` float(9,3) DEFAULT NULL,
  `IMG_Muscle` float(9,3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb3 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_poids`
--

LOCK TABLES `t_poids` WRITE;
/*!40000 ALTER TABLE `t_poids` DISABLE KEYS */;
INSERT INTO `t_poids` VALUES (1,'2022-08-18',106.200,4,35.480,0.000,0.000,NULL,NULL,NULL),(2,'2022-08-21',105.200,4,35.150,-1.000,-1.000,NULL,NULL,NULL),(3,'2022-08-22',104.200,3,34.820,-1.000,-2.000,NULL,NULL,NULL),(4,'2022-08-23',104.600,3,34.950,0.400,-1.600,NULL,NULL,NULL),(5,'2022-08-24',104.600,3,34.950,0.000,-1.600,NULL,NULL,NULL),(6,'2022-08-25',103.700,3,34.650,-0.900,-2.500,NULL,NULL,NULL),(7,'2022-08-26',103.700,3,34.650,0.000,-2.500,NULL,NULL,NULL),(8,'2022-08-27',104.000,3,34.750,0.300,-2.200,NULL,NULL,NULL),(9,'2022-08-28',103.600,3,34.620,-0.400,-2.600,NULL,NULL,NULL),(10,'2022-08-29',103.600,3,34.620,0.000,-2.600,NULL,NULL,NULL),(11,'2022-08-30',103.100,3,34.450,-0.500,-3.100,NULL,NULL,NULL),(12,'2022-08-31',101.100,3,33.780,-2.000,-5.100,NULL,NULL,NULL),(13,'2022-09-01',100.200,3,33.480,-0.900,-6.000,NULL,NULL,NULL),(14,'2022-09-02',99.900,3,33.380,-0.300,-6.300,NULL,NULL,NULL),(15,'2022-09-03',99.900,3,33.380,0.000,-6.300,NULL,NULL,NULL),(16,'2022-09-04',99.300,3,33.180,-0.600,-6.900,NULL,NULL,NULL),(17,'2022-09-05',99.400,3,33.210,0.100,-6.800,NULL,NULL,NULL),(18,'2022-09-06',99.600,3,33.280,0.200,-6.600,43.300,44.400,53.700),(19,'2022-09-07',98.700,3,32.980,-0.900,-7.500,43.300,44.400,53.700),(20,'2022-09-08',98.900,3,33.040,0.200,-7.300,42.800,44.500,54.200),(21,'2022-09-09',99.300,3,33.180,0.400,-6.900,43.500,44.400,53.500),(22,'2022-09-10',99.400,3,33.210,0.100,-6.800,42.100,44.700,54.800),(23,'2022-09-11',99.600,3,33.280,0.200,-6.600,42.100,44.700,54.800),(24,'2022-09-12',98.700,3,32.980,-0.900,-7.500,43.000,44.500,54.000),(25,'2022-09-13',98.300,3,32.840,-0.400,-7.900,45.300,43.900,51.700),(26,'2022-09-14',98.200,3,32.810,-0.100,-8.000,44.100,44.200,52.900),(27,'2022-09-15',98.300,3,32.840,0.100,-7.900,45.000,44.000,52.000),(28,'2022-09-16',98.200,3,32.810,-0.100,-8.000,44.200,44.200,52.800),(29,'2022-09-17',98.400,3,32.880,0.200,-7.800,43.300,44.400,53.700),(30,'2022-09-18',98.100,3,32.780,-0.300,-8.100,44.700,44.100,52.300),(31,'2022-09-19',98.700,3,32.980,0.600,-7.500,44.100,44.200,52.900),(32,'2022-09-20',97.700,3,32.640,-1.000,-8.500,46.100,43.700,50.900),(33,'2022-09-21',97.400,3,32.540,-0.300,-8.800,45.500,43.900,51.500),(34,'2022-09-22',97.400,3,32.540,0.000,-8.800,43.900,44.300,53.100),(35,'2022-09-23',96.800,3,32.340,-0.600,-9.400,43.300,44.400,53.700),(36,'2022-09-24',97.000,3,32.410,0.200,-9.200,45.800,4.800,51.300),(37,'2022-09-25',96.600,3,32.280,-0.400,-9.600,43.200,44.500,53.700),(38,'2022-09-26',96.500,3,32.240,-0.100,-9.700,42.300,44.700,54.700),(39,'2022-09-27',96.700,3,32.310,0.200,-9.500,45.000,44.000,52.000),(40,'2022-09-28',97.300,3,32.510,0.600,-8.900,42.400,44.700,54.500),(41,'2022-09-29',97.300,3,32.510,0.000,-8.900,42.500,44.600,54.400),(42,'2022-09-30',96.700,3,32.310,-0.600,-9.500,42.000,44.800,54.900),(43,'2022-10-01',96.600,3,32.280,-0.100,-9.600,42.800,44.600,54.100),(44,'2022-10-02',96.900,3,32.380,0.300,-9.300,43.700,44.300,53.200),(45,'2022-10-03',96.700,3,32.310,-0.200,-9.500,44.000,44.300,53.000),(46,'2022-10-04',96.800,3,32.340,0.100,-9.400,43.800,44.300,53.200),(47,'2022-10-05',97.100,3,32.440,0.300,-9.100,43.400,44.400,53.000),(48,'2022-10-06',96.700,3,32.310,-0.400,-9.500,43.100,44.500,53.800),(49,'2022-10-07',96.500,3,32.240,-0.200,-9.700,42.500,44.600,54.400);
/*!40000 ALTER TABLE `t_poids` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-07 11:39:10
